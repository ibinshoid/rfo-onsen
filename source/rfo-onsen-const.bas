!===   G L O B A L   C O N S T A N T S
!===   For use by Include and Calling Programs

!===   O R I E N T A T I O N
onLANDSCAPE=0
onPORTRAIT=1

!===   S T A T U S   B A R
onSHOWSTATUSBAR=1
onHIDESTATUSBAR=0

!===   C O N T R O L S
onDISPLAY=1
onINPUT=2
onTEXT=3
onSELECT=4
onDATE=5
onBUTTON=6
onPICTURE=7
onLABEL=8
onLISTBOX=9
onCHECKBOX=10
onOPTBUTTON=11
onSPINBUTTON=12
onFRAME=13
onSHAPE=14
onCOMBOBOX=15
onTIME=16
onCHKBUTTON=17
onTOOLBAR=18

!===   S T Y L E S
onALIGNLEFT$="a"
onALIGNCENTRE$="b"
onALIGNRIGHT$="c"
onEDITABLE$="d"
onNOBORDER$="e"

!===   G E S T U R E S
onSWIPELEFT$="p"
onSWIPERIGHT$="q"
onSWIPEUP$="r"
onSWIPEDOWN$="s"
onTOUCH$="t"
onHOLD$="u"
onTAP$="v"
onDOUBLETAP$="w"
onPINCHIN$="x"
onPINCHOUT$="y"
onROTATE$="z"

!===   T O O L B A R
onMENUBTN$="A"
onBACKBTN$="B"

!===   L I S T B O X
onCHECKBOX$="X"

!===   I N P U T
onNUMBER$="A"
onPASSWORD$="B"
onDATE$="C"
onTIME$="D"
onTEXT$="E"
onTEXTAREA$="F"
!===   P I C T U R E
onGRAYSCALE$="A"
!===   V A R I O U S
onRECBREAK$=CHR$(174) % Registered Sign
onCOLBREAK$=CHR$(169) % Copyright Symbol
onFLDBREAK$=CHR$(183) % Middle Top
onCRLF$=CHR$(13)+CHR$(10)
onLF$=CHR$(10)
onSTRCRLF$="^^"
onDBLQUOTE$=CHR$(34)
onBACKSLASH$=CHR$(92)
onPOUNDSIGN$=CHR$(163)
onNOTSIGN$=CHR$(172)
onBLANK$="--Blank--"
